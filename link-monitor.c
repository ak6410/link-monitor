#include <errno.h>
#include <stdio.h>
#include <memory.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <linux/rtnetlink.h>
#include <sys/types.h>
#include <unistd.h>
#include <err.h>
#include <syslog.h>
#include <sys/wait.h>
#include <stdlib.h>
#include "version.h"

/* Монитор состояния сетевого соединения
 * Основано на https://habr.com/ru/articles/121254/
 */

int	run_as_daemon = 0 ;

char *prog_name ;

#define ST_IFMASK	(7)
#define ST_IFUNDEF	(1)
#define ST_IFUP		(2)
#define ST_IFDN		(4)
#define ST_LINKMASK	(7<<3)
#define ST_LINKUNDEF	(1<<3)
#define ST_LINKUP	(2<<3)
#define ST_LINKDN	(4<<4)

#define CD_ACTIVE	(1
#define CD_FINISHED	2

#define LM_SYSLOG 1
#define LM_STDOUT 2

typedef struct if_action_s {
    char 		*ifname ;
    char 		*action ;
    unsigned 	conf_log ;
    struct if_action_s *next ;
    unsigned	rt_saved_state ;
} if_action_t ;

enum lm_event_e {
    ET_DEL_ADDR = 0,
    ET_DEL_LINK,
    ET_NEW_LINK,
    ET_NEW_ADDR,
    ET_MAX_EVT
} ;

static char *lm_event_name[] = {
    "DEL_ADDR",
    "DEL_LINK",
    "NEW_LINK",
    "NEW_ADDR",
    "Uknown"
} ;


if_action_t if_list = { .ifname=NULL, .action=NULL, .conf_log=0, .next=NULL } ;

int default_log = (LM_SYSLOG|LM_STDOUT) ;

int log_state(int log_flags, char *ifname, char *ifstate, char *linkstate, int type) ;
if_action_t *list_get_next(if_action_t *start, char *ifname) ;
if_action_t *list_append(char *ifname) ;
void usage(void) ;
void parseRtattr(struct rtattr *tb[], int max, struct rtattr *rta, int len) ;
void do_exec(char *action, char *ifname, char *ifstate, char *linkstate, int type) ;
void exec_actions(char *ifname, char *ifstate, char *linkstate, int type, unsigned new_state) ;

/*
 * небольшая вспомогательная функция, которая с помощью макросов
 * netlink разбирает сообщение и помещает блоки данных в массив
 * атрибутов rtattr
 */
void parseRtattr(struct rtattr *tb[], int max, struct rtattr *rta, int len)
{
    memset(tb, 0, sizeof(struct rtattr *) * (max + 1));

    while (RTA_OK(rta, len)) {	// пока сообщение не закончилось
        if (rta->rta_type <= max) {
            tb[rta->rta_type] = rta;	//читаем атрибут
        }
        rta = RTA_NEXT(rta,len);	// получаем следующий атрибут
    }
}

void usage(void)
{
    fprintf(stderr,
            "%s - program to monitor network interfaces status. Version " VERSION "\n"
            "Usage:\n"
            "    %s [log-switches [interface-options]] ...\n\n"
            "        log-switches are used to control logging and output\n"
            "        log controls can appear many times in the command line\n"
            "        and the new log state will be applied to subsequent\n"
            "        interfaces.\n"
            "        Options:\n"
            "            --stdout turns on output to the stdout (default)\n"
            "            --no-stdout turns off output to the stdout\n"
            "            --log turns on syslog (default)\n"
            "            --no-log turns off syslog\n"
            "            --daemon run as a daemon\n"
            "\n"
            "        interface-options:\n"
            "            --if interface-name [exec 'shell command line statement']\n"
            "\n"
            "            Options can appear many times in the command line\n"
            "            Following environment variables will be set for shell command:\n"
            "              IFACE - interface name (for ex., eth0)\n"
            "              IFST - interface state (UP or DOWN)\n"
            "              LINKST - link state (ON or OFF)\n"
            "              LM_EVENT - type of event (DEL_ADDR, DEL_LINK, NEW_ADDR, NEW_LINK)\n"
            "\n"
            "Example:\n"
            "    %s --no-log --no-stdout --if eth1 exec 'ip link show dev $IFACE' \\\n"
            "        --log --if eth0 exec 'ip link show dev $IFACE' \\\n"
            "        --if eth0 exec 'test \"$LINK_ST\" = \"OFF\" && reboot' delay 20\n"
            "\n"
            "Turn off any logging, for eth1 show ip link; turn on syslog,\n"
            "show eth0 link state, reboot if eth0 link is down after delay of 2s\n",
            prog_name, prog_name, prog_name) ;
}

if_action_t *list_append(char *ifname)
{
    if_action_t *p, *n ;
    n = malloc(sizeof(if_action_t)) ;
    memset(n, 0, sizeof(if_action_t)) ;
    n->ifname = strdup(ifname) ;
    n->conf_log = default_log ;
    n->rt_saved_state = ST_IFUNDEF | ST_LINKUNDEF ;
    p = &if_list ;
    while(p->next) p = p->next ;
    p->next = n ;
    return n ;
}

if_action_t *list_get_next(if_action_t *start, char *ifname)
{
    if_action_t *p ;
    if (NULL == start) {
        p = if_list.next ;
    } else {
        p = start->next ;
    }
    while (p) {
        if (!strcmp(ifname, p->ifname)) {
            return p ;
        }
        p = p->next ;
    }
    return NULL ;
}

int log_state(int log_flags, char *ifname, char *ifstate, char *linkstate, int type)
{
    int logged = 0 ;
    if ((log_flags & LM_STDOUT) && !run_as_daemon) {
        switch (type) {
        case RTM_DELADDR:
            printf("%s del-addr\n", ifname);
            break;
        case RTM_DELLINK:
            printf("%s del-if\n", ifname);
            break;
        case RTM_NEWLINK:
            printf("%s if-state %s link-state %s\n", ifname, ifstate, linkstate);
            break;
        case RTM_NEWADDR:
            printf("%s new-addr\n", ifname) ;
            break;
        }
        logged |= LM_STDOUT ;
    }
    if (log_flags & LM_SYSLOG) {
        switch (type) {
        case RTM_DELADDR:
            syslog(LOG_INFO, "%s del-addr", ifname);
            break;
        case RTM_DELLINK:
            syslog(LOG_INFO, "%s del-if", ifname);
            break;
        case RTM_NEWLINK:
            syslog(LOG_INFO, "%s if-state %s link-state %s\n", ifname, ifstate, linkstate);
            break;
        case RTM_NEWADDR:
            printf("%s new-addr %s\n", ifname) ;
            break;
        }
        logged |= LM_SYSLOG ;
    }
    return logged ;
}

void do_exec(char *action, char *ifname, char *ifstate, char *linkstate, int type)
{
    int wstatus ;

    if (0 == fork()) {
        int rc ;
        int et ;
        daemon(1, 1) ;
        setenv("IFACE", ifname, 1);
        setenv("IFST", ifstate, 1);
        setenv("LINKST", linkstate, 1);
        switch (type) {
        case RTM_DELADDR:
            et = ET_DEL_ADDR ;
            break;
        case RTM_DELLINK:
            et = ET_DEL_LINK ;
            break;
        case RTM_NEWLINK:
            et = ET_NEW_LINK ;
            break;
        case RTM_NEWADDR:
            et = ET_NEW_ADDR ;
            break;
        default:
            et = ET_MAX_EVT ;
            break ;
        }
        setenv("LM_EVENT", lm_event_name[et], 1) ;
        syslog(LOG_INFO, "Execute action `%s` for %s %s %s", action, ifname, ifstate, lm_event_name[et]) ;
        rc = system(action) ;
        if (rc == -1) {
            syslog(LOG_ERR, "Can't create child while executing `%s` for %s", action, ifname) ;
        } else if (rc == 127) {
            syslog(LOG_ERR, "shell can't exec `%s` for %s", action, ifname) ;
        } else if (rc) {
            syslog(LOG_ERR, "Program `%s` for %s exits with error code %d",  action, ifname, rc) ;
        } else {
            syslog(LOG_INFO, "Program `%s` for %s finished",  action, ifname) ;
        }
        exit(0) ;
    }
    wait(&wstatus) ;
}

void exec_actions(char *ifname, char *ifstate, char *linkstate, int type, unsigned new_state)
{
    int logged = 0 ;
    if_action_t *p ;

    if (!if_list.next) {
        (void)log_state(default_log, ifname, ifstate, linkstate, type) ;
        return ;
    }
    p = NULL ;
    while (NULL != (p = list_get_next(p, ifname))) {
        unsigned lf ;
        lf = p->conf_log ^ logged ;
        lf &= p->conf_log ;
        if (lf) {
            logged |= log_state(lf, ifname, ifstate, linkstate, type) ;
        }
        if (p->action) {
            do_exec(p->action, ifname, ifstate, linkstate, type) ;
        }
    }
}


int main(int ac, char **av)
{
    struct sockaddr_nl	local;	/* локальный адрес */
    char buf[8192];			/* буфер сообщения */
    struct iovec iov;		/* структура сообщения */
    struct msghdr msg;
    int fd ;

    {
        prog_name = av[0] ;
        av++ ;
        ac-- ;
        while (*av) {
            if (!strcmp(*av, "-h") ||
                !strcmp(*av, "--help") ||
                !strcmp(*av, "help")) {
                usage() ;
                exit(0) ;
            }
            if (!strcmp(*av, "-S") ||
                !strcmp(*av, "--stdout")) {
                default_log |= LM_STDOUT ;
                ac-- ;
                av++ ;
                continue ;
            }
            if (!strcmp(*av, "-s") ||
                !strcmp(*av, "--no-stdout") ||
                !strcmp(*av, "--nostdout")) {
                default_log &= ~LM_STDOUT ;
                ac-- ;
                av++ ;
                continue ;
            }
            if (!strcmp(*av, "-L") ||
                !strcmp(*av, "--syslog") ||
                !strcmp(*av, "--log")) {
                default_log |= LM_SYSLOG ;
                ac-- ;
                av++ ;
                continue ;
            }
            if (!strcmp(*av, "-l") ||
                !strcmp(*av, "--no-syslog") ||
                !strcmp(*av, "--nosyslog") ||
                !strcmp(*av, "--nolog")) {
                default_log &= ~LM_SYSLOG ;
                ac-- ;
                av++ ;
                continue ;
            }

            if (!strcmp(*av, "-d") ||
                !strcmp(*av, "--daemon")) {
                default_log &= ~LM_STDOUT ;
                run_as_daemon++ ;
                ac-- ;
                av++ ;
                continue ;
            }

            if (!strcmp(*av, "-i") ||
                !strcmp(*av, "--if")) {
                if_action_t *p ;
                ac-- ;
                av++ ;
                if (!*av) {
                    errx(9, "Syntax error in command line: interface name mast follows --if\n") ;
                }
                p = list_append(*av) ;
                ac-- ;
                av++ ;
                while (*av) {
                    if (!strcmp(*av, "exec")) {
                        ac-- ;
                        av++ ;
                        if (!*av || **av == '-') {
                            errx(10, "Syntax error in command line: command must follows exec keyword\n") ;
                        }
                        if (p->action) {
                            p = list_append(p->ifname) ;
                        }
                        p->action = strdup(*av) ;
                        ac-- ;
                        av++ ;
                        continue ;
                    }
                    if (**av == '-') {
                        break ;
                    }

                }
            }

        }
    }

    if (run_as_daemon) {
        if (fork()) {
            exit(0) ;
        }
        daemon(0,0) ;
    }
    openlog("[log-monitor]", LOG_PID, LOG_LOCAL3);

    fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);

    if (fd < 0) {
        int e = errno ;
        syslog(LOG_ERR, "Error creating netlink socket: %s", (char*)strerror(errno)) ;
        errno = e ;
        err(1, "Error creating netlink socket: ") ;
    }
    iov.iov_base = buf;
    iov.iov_len = sizeof(buf);

    memset(&local, 0, sizeof(local));

    local.nl_family = AF_NETLINK;
    local.nl_groups = RTMGRP_LINK ;
    local.nl_pid = getpid();

    msg.msg_name = &local;
    msg.msg_namelen = sizeof(local);
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    if (bind(fd, (struct sockaddr*)&local, sizeof(local)) < 0) {
        int e = errno ;
        syslog(LOG_ERR, "Binding error with netlink sock: %s", (char*)strerror(errno)) ;
        errno = e ;
        err(2, "Binding error with netlink socket: ") ;
    }

    // читаем и разбираем сообщения из сокета
    while (1) {
        ssize_t status = recvmsg(fd, &msg, 0);
        if (status < 0) {
            int e = errno ;
            switch (errno) {
            case EINTR:
            case EAGAIN:
                continue ;
            default:
                syslog(LOG_ERR, "Error receiving message: %s", (char*)strerror(errno)) ;
                errno = e ;
                err(3, "Error receiving message: ") ;
            }
        }

        if (msg.msg_namelen != sizeof(local)) {
            syslog(LOG_WARNING, "Wrong sender address length") ;
            warnx("Wrong sender address length\n") ;
            continue;
        }

        struct nlmsghdr *h; /* Pointer to message header */

        for (h = (struct nlmsghdr*)buf; status >= (ssize_t)sizeof(*h); ) {
            int len = h->nlmsg_len;
            int l = len - sizeof(*h);
            char *ifName;

            if ((l < 0) || (len > status)) {
                syslog(LOG_WARNING, "Wrong message length") ;
                warnx("Wrong message length\n") ;
                continue;
            }
            {
                char *ifUpp;		/* Device status */
                char *ifRunn;		/* Link status */
                struct ifinfomsg *ifi;  /* Pointer to link info status */
                struct rtattr *tb[IFLA_MAX + 1];	/* attributes array, IFLA_MAX see in rtnetlink.h */
                unsigned state = 0 ;

                ifi	= (struct ifinfomsg*) NLMSG_DATA(h);

                parseRtattr(tb, IFLA_MAX, IFLA_RTA(ifi), h->nlmsg_len);

                if (tb[IFLA_IFNAME]) {
                    ifName = (char*)RTA_DATA(tb[IFLA_IFNAME]);
                }

                if (ifi->ifi_flags & IFF_UP) { // получаем состояние флага UP для соединения
                    ifUpp = (char*)"UP";
                    state |= ST_IFUP ;
                } else {
                    ifUpp = (char*)"DOWN";
                    state |= ST_IFDN ;
                }

                if (ifi->ifi_flags & IFF_RUNNING) { // получаем состояние флага RUNNING для соединения
                    ifRunn = (char*)"ON";
                    state |= ST_LINKUP ;
                } else {
                    ifRunn = (char*)"OFF";
                    state |= ST_LINKDN ;
                }

                char ifAddress[256];
                struct ifaddrmsg *ifa;
                struct rtattr *tba[IFA_MAX+1];

                ifa = (struct ifaddrmsg*)NLMSG_DATA(h);
                parseRtattr(tba, IFA_MAX, IFA_RTA(ifa), h->nlmsg_len);
                if (tba[IFA_LOCAL]) {
                    inet_ntop(AF_INET, RTA_DATA(tba[IFA_LOCAL]), ifAddress, sizeof(ifAddress));
                }
                exec_actions(ifName, ifUpp, ifRunn, h->nlmsg_type, state) ;
            }

            status -= NLMSG_ALIGN(len);
            h = (struct nlmsghdr*)((char*)h + NLMSG_ALIGN(len));
        }
    }
    close(fd);	// закрываем дескриптор сокета
    return 0;
}
